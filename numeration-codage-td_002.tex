% Travail dirigé - Codage des nombres
% Auteur  : Gregory DAVID
%%

\documentclass[12pt,a4paper,oneside,final]{exam}

\usepackage{style/layout}
\usepackage{style/commands}
\usepackage{style/glossaire}

\newcommand{\MONTITRE}{Codage des nombres}
\newcommand{\MONSOUSTITRE}{travail dirigé}
\newcommand{\DISCIPLINE}{\gls{SiUN}}

\usepackage[%
pdftex,%
pdfpagelabels=true,%
pdftitle={\MONTITRE},%
pdfauthor={Grégory DAVID},%
pdfsubject={\MONSOUSTITRE},%
colorlinks,%
]{hyperref}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

%\printanswers\groolotPhiligranne{CORRECTION}

\begin{document}
% Page de titre
\maketitle

% Copyright
\input{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\section*{Codage d'un nombre décimal signé en binaire selon la
  norme \texttt{IEEE 754}\cite{wiki:IEEE754}}
\begin{questions}
    \question Démontrer que les représentations en binaire des
    nombres décimaux suivants sont correctes sur \lstinline{32bits}
    :
    \begin{parts}
        \part
        $$ \overline{1.0}_{10} = \overline{00111111100000000000000000000000}_{2} $$
        \begin{solution}
            \begin{proof}
                $$ \textnormal{en écriture décimale binaire on obtient } \overline{1.0}_{10} = 1 \times 2^{0} = \overline{1.0}_{2} \textnormal{,} $$
                le nombre est normalisé
                $ \overline{1.0}_{2} = S \times 1.M \times 2^{e} $,
                o\`u
                $$ S = \overline{0}_{2} \textnormal{, car le signe est positif} $$
                $$ e = 0 \Rightarrow E = e + 127 = 127 = \overline{01111111}_{2} $$
                $$ M = \overline{00000000000000000000000}_{2} $$
            \end{proof}
        \end{solution}

        \part
        $$ \overline{-8.5}_{10} = \overline{11000001000010000000000000000000}_{2} $$
        \begin{solution}
            \begin{proof}
                ~\\
                \begin{enumerate}
                    \item conversion binaire de la partie entière :
                    $$ \overline{8}_{10} = 1 \times 2^{3} = \overline{1000}_{2}$$
                    \item conversion binaire de la partie décimale :
                    \subitem la mantisse peut contenir
                    \lstinline{23bits}, or la partie entière
                    normalisée occupe déjà \lstinline{3bits} sur
                    l'espace de la mantisse, il nous reste donc
                    \lstinline{20bits} disponibles pour la partie
                    décimale : $$ 0.5 \times 2^{20} = 524288 $$ avec
                    l'arrondi à l'entier le plus proche, \subitem
                    nous n'avons plus qu'à convertir la valeur
                    $ \overline{524288}_{10} $ en binaire
                    \DivisionEuclidienneSuccessive{524288}{2}

                    \item récriture complète du nombre décimale
                    en binaire
                    $$ \overline{-8.5}_{10} = \overline{-1000.10000000000000000000}_{2}$$

                    \item le nombre n'est pas normalisé : normalisation\\
                    on veut le nombre sous la forme
                    $ S \times 1.M \times 2^{e} $
                    $$ \overline{-1000.10000000000000000000}_{2} = \overline{-1 \times 1.00010000000000000000000 \times 2^{3}}_{2}$$
                    o\`u
                    $$ S = \overline{1}_{2} \textnormal{, car le signe est négatif} $$
                    $$ e = 3 \Rightarrow E = e + 127 = 130 = \overline{10000010}_{2} $$
                    $$ M = \overline{00010000000000000000000}_{2} $$
                \end{enumerate}
            \end{proof}
        \end{solution}

        \part
        $$ \overline{-1024.837}_{10} = \overline{11000100100000000001101011001001}_{2} $$
        \begin{solution}
            \begin{proof}
                ~\\
                \begin{enumerate}
                    \item conversion binaire de la partie entière :
                    $$ \overline{1024}_{10} = 1 \times 2^{10} = \overline{10000000000}_{2} $$

                    \item conversion binaire de la partie décimale :
                    \subitem la mantisse peut contenir
                    \lstinline{23bits}, or la partie entière
                    normalisée occupe déjà \lstinline{10bits}
                    sur l'espace de la mantisse, il nous reste donc
                    \lstinline{13bits} disponibles pour la partie
                    décimale :
                    $$ 0.837 \times 2^{13} = 6856.704 \simeq 6857$$
                    avec l'arrondi à l'entier le plus proche,
                    \subitem nous n'avons plus qu'à convertir la
                    valeur $ \overline{6857}_{10} $ en binaire
                    \DivisionEuclidienneSuccessive{6857}{2}
                    $$ \overline{6857}_{10} = \overline{1101011001001}_{2} $$

                    \item récriture complète du nombre décimale
                    en binaire
                    $$ \overline{-1024.837}_{10} = \overline{-10000000000.1101011001001}_{2}$$

                    \item le nombre n'est pas normalisé : normalisation\\
                    on veut le nombre sous la forme
                    $ S \times 1.M \times 2^{e} $
                    $$ \overline{-10000000000.1101011001001}_{2} = \overline{-1 \times 1.00000000001101011001001 \times 2^{10}}_{2}$$
                    o\`u
                    $$ S = \overline{1}_{2} \textnormal{, car le signe est négatif} $$
                    $$ e = 10 \Rightarrow E = e + 127 = 137 = \overline{10001001}_{2} $$
                    $$ M = \overline{00000000001101011001001}_{2} $$
                \end{enumerate}
            \end{proof}
        \end{solution}
    \end{parts}

    \question Démontrer que les représentations en binaire des
    nombres décimaux suivants sont correctes sur \lstinline{64bits}
    :
    \begin{parts}
        \part
        $$ n = \overline{-1234.5678}_{10} $$ $$ n = \overline{1100000010010011010010100100010101101101010111001111101010101101}_{2} $$
        \begin{solution}
            \begin{proof}
                ~\\
                \begin{enumerate}
                    \item conversion binaire de la partie entière :
                    $$ \overline{1234}_{10} = 1 \times 2^{10} + 1 \times 2^{7} + 1 \times 2^{6} + 1 \times 2^{4} + 1 \times 2^{1} = \overline{10011010010}_{2} $$

                    \item conversion binaire de la partie décimale :
                    \subitem la mantisse peut contenir
                    \lstinline{52bits}, or la partie entière
                    normalisée occupe déjà \lstinline{10bits}
                    sur l'espace de la mantisse, il nous reste donc
                    \lstinline{42bits} disponibles pour la partie
                    décimale :
                    $$ 0.5678 \times 2^{40} = 624302702251.2128 \simeq
                    624302702251 $$
                    avec l'arrondi à l'entier le plus proche,
                    \subitem nous n'avons plus qu'à convertir la
                    valeur $ \overline{624302702251}_{10} $ en binaire
                    \DivisionEuclidienneSuccessive{624302702251}{2}
                    $$ \overline{624302702251}_{10} = \overline{100100010101101101010111001111101010101101}_{2}$$

                    \item récriture complète du nombre décimale
                    en binaire
                    $$ \overline{-1234.5678}_{10} = \overline{-10011010010.100100010101101101010111001111101010101101}_{2}$$

                    \item le nombre n'est pas normalisé : normalisation\\
                    on veut le nombre sous la forme
                    $ S \times 1.M \times 2^{e} $
                    $$ N = \overline{-10011010010.100100010101101101010111001111101010101101}_{2} $$
                    $$ N = \overline{-1 \times 1.0011010010100100010101101101010111001111101010101101 \times 2^{10}}_{2}$$
                    o\`u
                    $$ S = \overline{1}_{2} \textnormal{, car le signe est négatif} $$
                    $$ e = 10 \Rightarrow E = e + 1023 = 1033 = \overline{10000001001}_{2} $$
                    $$ M = \overline{0011010010100100010101101101010111001111101010101101}_{2} $$
                \end{enumerate}
            \end{proof}
        \end{solution}
    \end{parts}
\end{questions}

% References bibliographiques
\printbibheading
% \printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}]
\printbibliography[check=online,heading=subbibliography,title={Webographiques}]

\end{document}

