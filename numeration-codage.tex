% Numération et codage
%
% Auteur : Grégory DAVID
%%

\documentclass[12pt,a4paper,oneside,final]{exam}

\usepackage{style/layout}
\usepackage{style/commands}
\usepackage{style/glossaire}

\usepackage{amsfonts}
\usepackage{amsmath}

\newcommand{\MONTITRE}{Numération \& Codage}
\newcommand{\MONSOUSTITRE}{}
\newcommand{\DISCIPLINE}{\Gls{SiUN}}

\usepackage[%
pdftex,%
pdfpagelabels=true,%
pdftitle={\MONTITRE},%
pdfauthor={Grégory DAVID},%
pdfsubject={\MONSOUSTITRE},%
colorlinks,%
]{hyperref}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

%\printanswers

\begin{document}
% Page de titre
\maketitle
\vspace{\fill}
\tableofcontents{}
\vspace{\fill}

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\section{Numération}
\subsection{Les entiers naturels}
\begin{quotation}
  En mathématiques, un entier naturel est un nombre positif (ou
  nul\footnote{La conception de zéro comme nombre positif et comme
    entier naturel est récente et dépend encore de conventions,
    contestées notamment dans les pays anglo-saxons}) permettant
  fondamentalement de dénombrer des objets comptant chacun pour un. Un
  tel nombre entier peut s'écrire avec une suite finie de chiffres en
  notation décimale positionnelle, sans signe et sans partie
  fractionnaire, c'est-à-dire sans chiffre \og~après la virgule~\fg.

  Les entiers naturels sont donc, outre zéro, ceux que l'on commence à
  énumérer avec la comptine numérique : \emph{un, deux, trois, quatre
    \dots{}} Mais la liste des entiers naturels est infinie, car
  chacun d'entre eux a un successeur, c'est-à-dire un entier qui lui
  est immédiatement supérieur.

  L'ensemble des entiers naturels est noté
  $\mathbb{N}$.\cite{wiki:ENTIER_NATUREL}
\end{quotation}

Soit $b \in \mathbb{N}^*$.

Quel que soit l'entier naturel $x$, il existe une et une seule suite
d'entiers $ a_n, \ldots, a_1, a_0 $ avec
\[ 0 \leq a_i < b \]
\[ 0 \leq i \leq n \textnormal{ où } 0 \leq n \]

telle que :
\[ x = a_n \times b^n + a_{n-1} \times b^{n-1} + \cdots + a_1 \times
  b^1 + a_0 \times b^0 \] ou bien (avec la rigueur mathématique) :
\[ x = \sum_{i=0}^{n} a_i \times b^i \] On dit alors que $ b $ est une
base et que $ a_n,a_{n-1}, \ldots, a_1, a_0 $ est le développement de
$ x $ dans la base $ b $ et on écrit :
\[ x = \overline{a_{n}a_{n-1}\cdots a_1a_0}_b \]

\subsubsection{Exemples}
\paragraph{conversion d'une base 10 à une base $ b=10 $}
en base 10, 28 s'écrit comme :
\[ \overline{28}_{10} = 2 \times 10^1 + 8 \times 10^0 \]

en effet : \DivisionEuclidienneSuccessive{28}{10}
\paragraph*{conversion d'une base 10 à une base $ b=8 $}
en base 8, 134 s'écrit comme :
\[ \overline{134}_{10} = 2 \times 8^2 + 0 \times 8^1 + 6 \times 8^0 =
  \overline{206}_8 \]

en effet : \DivisionEuclidienneSuccessive{134}{8}
\paragraph*{conversion d'une base 10 à une base $ b=2 $}
en base 2, 77 s'écrit comme :
\[ \overline{77}_{10} = 1 \times 2^6 + 0 \times 2^5 + 0 \times 2^4 + 1
  \times 2^3 + 1 \times 2^2 + 0 \times 2^1 + 1 \times 2^0 =
  \overline{1001101}_2 \]

en effet : \DivisionEuclidienneSuccessive{77}{2}
\subsubsection{Exercices}
Convertir les nombres suivants dans la base proposée
\begin{questions}
  \question{} $ \overline{65}_{10} = \overline{????}_{2} $
  \begin{solution}
    \[ \overline{65}_{10} = 1 \times 2^{6} + 1 \times 2^{0} =
      \overline{1000001}_{2} \] en effet :
    \DivisionEuclidienneSuccessive{65}{2}
  \end{solution}

  \question{} $ \overline{1234}_{10} = \overline{????}_{2} $
  \begin{solution}
    \[ \overline{1234}_{10} = 1 \times 2^{10} + 1 \times 2^{7}+ 1
      \times 2^{6}+ 1 \times 2^{4}+ 1 \times 2^{1} =
      \overline{10011010010}_{2} \] en effet :
    \DivisionEuclidienneSuccessive{1234}{2}
  \end{solution}

  \question{} $ \overline{345}_{10} = \overline{????}_{3} $
  \begin{solution}
    \[ \overline{345}_{10} = 1 \times 3^{5} + 1 \times 3^{4} + 2
      \times 3^{2} + 1 \times 3^{1} = \overline{110210}_{3} \] en
    effet : \DivisionEuclidienneSuccessive{345}{3}
  \end{solution}

  \question{} $ \overline{654}_{10} = \overline{????}_{16} $
  \begin{solution}
    \[ \overline{654}_{10} = 2 \times 16^{2} + 8 \times 16^{1} + E
      \times 16^{0} = \overline{28E}_{16} \] en effet :
    \DivisionEuclidienneSuccessive{654}{16}
  \end{solution}

  \question{} en utilisant les caractères alphabétiques comme chiffres
  possibles ($A = 0$ et $Z = 25$) :
  $ \overline{44991}_{10} = \overline{????}_{26} $
  \begin{solution}
    \[ \overline{44991}_{10} = C \times 26^{3} + O \times 26^{2} + O
      \times 26^{1} + L \times 26^{0} = \overline{COOL}_{26} \] en
    effet : \DivisionEuclidienneSuccessive{44991}{26}
  \end{solution}
\end{questions}

\subsection{Les entiers relatifs}
\begin{quotation}
  En mathématiques, un entier relatif se présente comme un entier
  naturel muni d'un signe positif ou négatif qui indique sa
  position\footnote{De cette position relative à zéro vient l'adjectif
    \og~relatif~\fg appliqué à ces entiers.} par rapport à zéro sur un
  axe orienté. Les entiers positifs (supérieurs à zéro) s'identifient
  aux entiers naturels : \emph{0, 1, 2, 3} \dots tandis que les
  entiers négatifs sont leur opposés : \emph{0, -1, -2, -3} \dots
  L'entier zéro lui-même est donc le seul nombre à la fois positif et
  négatif\footnote{Selon certaines conventions différentes, en vigueur
    notamment dans les pays anglo-saxons, l'entier zéro n'est ni
    positif ni négatif}.

  L'ensemble des entiers relatifs est noté
  $\mathbb{Z}$.\cite{wiki:ENTIER_RELATIF}
\end{quotation}

Ainsi les nombres entiers relatifs sont similaires aux entiers
naturels, à la différence qu'ils sont signés.


\subsection{Les nombres décimaux}
Un nombre décimal est un nombre possédant une quantité finie de
chiffres permettant de le décrire. Il ne faut cependant pas confondre
avec les nombres réels qui, comme $\pi $, ont une infinité de chiffres
; à ce titre, le nombre $3.14159$ est une approximation décimale du
nombre $\pi$.

Nous décrirons alors tout nombre décimal de la manière suivante :

Soient $b, n \in \mathbb{N}$ et $i, j, m \in \mathbb{Z}$.

Quel que soit le nombre décimal $x$, il existe une et une seule suite
d'entiers \[ a_n, \ldots, a_{1}, a_0, a_{-1}, \ldots, a_m \] avec
\[ 0 \leq a_i < b \]
\[ 0 \leq i \leq n\]
\[ m \leq j < 0 \textnormal{ où } m < 0\]

telle que :
\[ x = a_n \times b^n + a_{n-1} \times b^{n-1} + \cdots + a_1 \times
  b^1 + a_0 \times b^0 + a_{-1} \times b^{-1} + \cdots + a_{m+1}
  \times b^{m+1} + a_{m} \times b^{m} \] ou bien (avec la rigueur
mathématique) :
\[ x = \sum_{i=0}^{n} a_i \times b^i + \sum_{j=-1}^{m} a_j \times
  b^j \] On dit alors que $ b $ est une base et que
$ a_n,a_{n-1}, \ldots, a_1, a_0, a_{-1}, \ldots, a_{m+1}, a_{m} $ est
le développement de $ x $ dans la base $ b $ et on écrit :
\[ x = \overline{a_{n}a_{n-1}\ldots a_1a_0,a_{-1}\ldots
    a_{m+1}a_{m}}_b \]

On peut alors voir l'écriture d'un nombre décimal comme l'assemblage
de deux nombres entiers, l'un décrivant la partie entière du nombre
décimal ($ a_n,a_{n-1}, \ldots, a_1, a_0$), l'autre décrivant la
partie décimale de ce même nombre ($a_{-1}, \ldots, a_{m+1}, a_{m}$).

Nous considérons alors la partie entière du nombre décimal comme étant
la somme des puissances de $b$, ayant l'exposant $y \in \mathbb{N}$.

Nous considérons alors la partie décimale du nombre décimal comme
étant la somme des puissances de $b$, ayant l'exposant
$y \in \mathbb{Z}$ et $y < 0$.

\subsubsection{Exemples}
\paragraph{conversion d'une base 10 à une base $ b=10 $}
$1234,5678$ s'écrit comme :
\[ \overline{1234,5678}_{10} = 1 \times 10^3 + 2 \times 10^2 + 3
  \times 10^1 + 4 \times 10^0 + 5 \times 10^{-1} + 6 \times 10^{-2} +
  7 \times 10^{-3} + 8 \times 10^{-4} \] en effet :
\[ 1 \times 10^3 + 2 \times 10^2 + 3 \times 10^1 + 4 \times 10^0 =
  \overline{1234}_{10} \]
\[ 5 \times 10^{-1} + 6 \times 10^{-2} + 7 \times 10^{-3} + 8 \times
  10^{-4} = \overline{0,5678}_{10} \]

\paragraph{conversion d'une base 10 à une base $ b=2 $}
$12,625$ s'écrit comme :
\[ \overline{12,625}_{10} = 1 \times 2^3 + 1 \times 2^2 + 1 \times
  2^{-1} + 1 \times 2^{-3} \]
\[ \overline{12,625}_{10} = \overline{1100,101}_2 \] en effet :
\[ \overline{1100}_2 = 1 \times 2^3 + 1 \times 2^2 =
  \overline{12}_{10} \]
\[ \overline{0,101}_2 = 1 \times 2^{-1} + 1 \times 2^{-3} =
  \overline{0,625}_{10} \]

\paragraph{conversion d'une base 10 à une base $ b=16 $}
$12,625$ s'écrit comme :
\[ \overline{12,625}_{10} = C \times 16^0 + A \times 16^{-1} \]
\[ \overline{12,625}_{10} = \overline{C,A}_{16} \] en effet :
\[ \overline{C}_{16} = C \times 16^0 = 12 \times 1 =
  \overline{12}_{10} \]
\[ \overline{0,A}_{16} = A \times 16^{-1} = 10 \times 0,0625 =
  \overline{0,625}_{10} \]

\section{Regroupements binaires}
\begin{description}
  \item[quartet] \texttt{4bits}
  \item[octet, byte, char] \texttt{8bits}
  \item[short int] \texttt{16bits} (taille variable selon les
  architectures)
  \item[mot, word, int, float] \texttt{32bits} (taille variable
  selon les architectures)
  \item[long, double] \texttt{64bits} (taille variable selon les
  architectures)
\end{description}

Une mémoire est formée d'un certain nombre de cellules (ou cases ou
mots), où chacune de ces cellules est constituée par un nombre fixe de
bits. Suivant l'architecture, la taille du mot n'est pas la même.

On peut trouver des architectures où le mot est de 8bits
(\texttt{Intel 8080}\cite{wiki:INTEL_8080}), d'autres où il est de
16bits (\texttt{Zilog Z8000}\cite{wiki:ZILOG_Z8000}), 32bits
(\texttt{Motorola 68060}\cite{wiki:MOTOROLA_68060}), 64bits
(\texttt{AMD Athlon64}\cite{wiki:AMD_ATHLON64}, \texttt{IBM Cell
  (PlayStation3)}\cite{wiki:IBM_CELL_64}), 128bits (\texttt{Transmeta
  Crusoé}\cite{wiki:TRANSMETA_CRUSOE_128}) ou 256bits
(\texttt{Transmeta Efficeon}\cite{wiki:TRANSMETA_EFFICEON_256}). Comme
la longueur est dépendante de l'architecture, on a pris l'habitude de
mesurer la taille (le nombre de bits) des mémoires en nombre d'octets.

\clearpage
\section{Codage des nombres}
\subsection{Nombres entiers
  naturels}\label{sec:nombres-entiers-naturels}
Le codage d'un nombre entier naturel est simple. En effet, il suffit
de représenter sa valeur absolue\cite{wiki:VALEUR_ABSOLUE} binaire
dans l'espace disponible. Ainsi, en fonction de l'espace de stockage
mémoire, nous pourrons représenter toutes les valeurs telles que dans
les \tablename~\vref{tab_entier_naturel_8bits} et
\tablename~\vref{tab_entier_naturel_16bits}.
\tableau{|c|c|c|c|c|c|c|c|l}{ \cline{1-8} \multicolumn{8}{|c|}{Valeur
    absolue} \\ \cline{1-8} 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & $= 0$ \\
  \cline{1-8} . & . & . & . & . & . & . & . & \\
  \cline{1-8} 0 & 1 & 1 & 0 & 0 & 1 & 0 & 1 & $= 101$ \\ \cline{1-8}
  . & . & . & . & . & . &
  . & . & \\ \cline{1-8} 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & $= 255$ \\
  \cline{1-8} }{Codage d'un entier naturel sur
  \texttt{8bits}}{tab_entier_naturel_8bits}

\tableau{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|l}{ \cline{1-16}
  \multicolumn{16}{|c|}{Valeur absolue} \\ \cline{1-16} 0 & 0 & 0 & 0
  & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & $= 0$ \\
  \cline{1-16} . & . & . & . & . & . & . & . & . & . & . & . & . & . &
  . & . & \\ \cline{1-16} 0 & 1 & 1 & 0 & 0 & 1 & 0 & 1 & 0 & 1 & 1 &
  0 & 0 & 1 & 0 & 1 & $= 25957$ \\ \cline{1-16} . & . & . & . & . &
  . &
  . & . & . & . & . & . & . & . & . & . & \\
  \cline{1-16} 1 & 1 &
  1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & 1 & $= 65535$ \\
  \cline{1-16} }{Codage d'un entier naturel sur
  \texttt{16bits}}{tab_entier_naturel_16bits}

\subsection{Nombres entiers
  relatifs}\label{sec:nombres-entiers-relatifs}

\subsubsection{De façon intuitive}\label{sec:Z-de-facon-intuitive}
La distinction entre $-12$ et $12$ est seulement liée au signe. En
effet, la valeur absolue est identique pour chacun d'eux.

De ce fait, il est alors envisageable de choisir un élément simple
pour représenter le signe : le bit de poids fort\footnote{le premier
  bit de l'octet en partant de la gauche}.

Dans cette hypothèse, nous aurions alors notre \texttt{octet}
représenté tel que dans le
\tablename~\vref{tab_entier_relatif_8bits_intuitif}.
\tableau{|c|c|c|c|c|c|c|c|l}{ \cline{1-8} S &
  \multicolumn{7}{c|}{Valeur absolue} \\ \cline{1-8} 0 & 0 & 0 & 0 & 0
  & 0 & 0 & 0 & $= 0$\\ \cline{1-8} 0 & 0 & 0 & 0 & 1 & 1 & 0 & 0 & $=
  12$\\ \cline{1-8} 1 & 0 & 0 & 0 & 1 & 1 & 0 & 0 & $= -12$\\
  \cline{1-8} }{Codage d'un entier relatif sur \texttt{8bits} de
  fa\c{c}on intuitive}{tab_entier_relatif_8bits_intuitif}

Mais cette représentation possède deux inconvénients notables :
\begin{enumerate}
  \item il n'est pas possible de faire des opérations entre les
  nombres ainsi codés, telles que démontrées en
  \tablename~\vref{proof_operation_entiers_naturels},
  \item il y a une double représentation du nombre $0$, tels que dans
  \tablename~\vref{tab_entier_relatif_intuitif_8bits_double_0}.
\end{enumerate}

\tableau{r}{
  $00001100$ \\
  $+10001100$ \\ \hline
  $10011000$ \\
}{Tentative de réaliser le calcul $12 - 12 = 12 + (-12) = 0$, donnant
  $12 + (-12) = -24$ dans cette
  situation}{proof_operation_entiers_naturels}

\tableau{|c|c|c|c|c|c|c|c|l}{ \cline{1-8} S &
  \multicolumn{7}{c|}{Valeur absolue} \\ \cline{1-8} 0 & 0 & 0 & 0 & 0
  & 0 & 0 & 0 & $= 0$ \\ \cline{1-8} 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 &
  $= -0$ \\ \cline{1-8} }{Double codage de la valeur $0$, tant\^ot
  avec le nombre $+0$, tant\^ot avec le nombre
  $-0$}{tab_entier_relatif_intuitif_8bits_double_0}

\subsubsection{Complément à un}\label{sec:complement-a-un}
Le complément à un d'un nombre binaire est la valeur obtenue en
inversant tous les bits de ce nombre (en permutant les \texttt{0} par
des \texttt{1} et inversement). Le complément à un d'un nombre se
comporte alors comme le négatif du nombre original dans certaines
opérations arithmétiques.

D'un point de vue algébrique, qui est plus général, c'est l'opération
qui consiste à complémenter un nombre écrit en base \texttt{b} sur
\texttt{n} chiffres à $b^n-1$. C'est-à-dire que le complément d'un
nombre a s'obtient par $(b^n-1)-a$.

\paragraph{Exemple} Codage du nombre entier relatif négatif suivant,
sur \texttt{8bits} : $-12$.

\begin{enumerate}
  \item Codage de la valeur absolue sur \texttt{8bits} :
  $\overline{12}_{10}=\overline{00001100}_2$
  \item Complémentation :
  $\overline{00001100}_2 \rightarrow \overline{11110011}_2$
\end{enumerate}

\paragraph{\emph{Nota bene}} L'avantage de ce codage est qu'il est
simple à mettre en œuvre, cependant il a l'inconvénient de représenter
deux fois la valeur 0 : \texttt{00000000} et \texttt{11111111}.

Par ailleurs, il n'est pas cohérent arithmétiquement. En effet,
l'opération $20 + (-7)$ n'est pas satisfaisante, puisque le résultat
serait $12$. Voir la démonstration dans la
\tablename~\vref{proof_operation_entiers_naturels_complement_un}.

\tableau{r}{
  $00010100$ \\
  $+11111000$ \\ \hline
  $1'00001100$ \\
}{Tentative de réaliser le calcul $20 - 7 = 20 + (-7) = 13$, donnant
  $20 + (-7) = 12$ dans cette
  situation}{proof_operation_entiers_naturels_complement_un}

\clearpage
\subsubsection{Complément à deux}\label{sec:complement-a-deux}
Le complément à deux est une méthode et une norme de codage des
nombres entiers relatifs. L'idée principale étant de représenter des
nombres entiers relatifs en utilisant le minimum d'espace (8bits) et
permettant de faire des calculs. En effet, les méthodes intuitives
envisagées nous ont révélé quelques lacunes en la matière
(Cf. section~\vref{sec:Z-de-facon-intuitive} et
section~\vref{sec:complement-a-un}).

Ainsi, pour assurer le codage d'un nombre entier relatif négatif, nous
suivons la méthode suivante :
\begin{enumerate}
  \item Coder la valeur absolue du nombre négatif, voir
  section~\vref{sec:nombres-entiers-naturels}.
  \item Effectuer le complément à un du nombre, voir
  section~\vref{sec:complement-a-un}.
  \item Ajouter 1 à ce nombre.
\end{enumerate}

\paragraph{Exemple} Codons le nombre entier relatif négatif suivant
sur \texttt{8bits} : $-12$.

\begin{enumerate}
  \item Codage de la valeur absolue sur \texttt{8bits} :
  $\overline{12}_{10}=\overline{00001100}_2$
  \item Complémentation :
  $\overline{00001100}_2 \rightarrow \overline{11110011}_2$
  \item Ajout d'une unité :
  $\overline{11110011}_2+\overline{1}_2=\overline{11110100}_2$
\end{enumerate}

Ainsi, le codage du nombre entier relatif $-12$ sur \texttt{8bits} est
$\overline{11110100}_2$

\paragraph{\emph{Nota bene}}
La \tablename\ \vref{tab:TableCorrespondanceZ} présente un extrait des
représentations des nombres entiers relatifs sur \texttt{8bits}.

\tableau{|c|c|}{ \hline
  Valeur binaire sur 8bits & Représentation \\
  \hline \hline
  $00000000$ & $0$ \\
  $00000001$ & $1$ \\
  $00000010$ & $2$ \\
  \ldots & \ldots \\
  $01111110$ & $126$ \\
  $01111111$ & $127$ \\
  $10000000$ & $-128$ \\
  $10000001$ & $-127$ \\
  $10000010$ & $-126$ \\
  \ldots & \ldots \\
  $11111101$ & $-3$ \\
  $11111110$ & $-2$ \\
  $11111111$ & $-1$ \\ \hline }{Table de correspondance entre la
  valeur binaire absolue et le nombre entier relatif représenté selon
  la convention du complément à deux}{tab:TableCorrespondanceZ}

\subsection{Nombres décimaux, norme \texttt{IEEE
    754}\cite{wiki:IEEE754}.}
\subsubsection{Représentation sur \texttt{32bits}}
Démarche permettant la représentation d'un nombre selon la norme
\texttt{IEEE 754}\cite{wiki:IEEE754}.
\begin{enumerate}
  \item Convertir en binaire la partie entière du nombre décimal, en
  utilisant la méthode des divisions euclidiennes successives du
  nombre par 2 ou celle de la somme des puissances positives de 2.
  \item Convertir en binaire la partie décimale du nombre décimal, en
  utilisant la méthode des divisions euclidiennes successives
  appliquée au nombre décimal décalé d'autant de bits à droite (donc
  multiplié par $2^n$ où $n$ est le nombre de bits disponible) qu'il y
  en aura de disponible dans la mantisse (à adapter pour chaque
  nombre)
  \item Normaliser le nombre binaire $N$ pour qu'il soit :
  $ \overline{1.0}_2 \leq N < \overline{10.0}_2$. Par exemple le
  nombre binaire $\overline{0.000101}_2$ sera normalisé
  $\overline{1.01}_2 \times 2^{-4}$
  \item Remplir la mantisse avec tous les chiffres binaires à droite
  de la virgule. Par exemple, pour le nombre normalisé
  $\overline{1.01}_2 \times 2^{-4}$ la mantisse sera :
  \[ m = \overline{01000000000000000000000}_2 \]
  \item Identifier le signe et le valoriser dans le bit de poids fort
  ($+ \equiv \overline{0}_2$ et $- \equiv \overline{1}_2$)
  \item calculer la valeur de l'exposant à stocker en fonction de
  l'exposant réel décalé de $+127$
\end{enumerate}

\paragraph{Exemple}
Faire la conversion en binaire selon la norme du nombre décimale écrit
en base dix $\overline{-0.1234}_{10}$ selon la norme \texttt{IEEE
  754}\cite{wiki:IEEE754}.
\begin{proof}~\newline
  \begin{enumerate}
    \item Conversion binaire de la partie entière :
    \[ \overline{0}_{10} = 0 \times 2^{0} = \overline{0}_{2}\]

    \item Conversion binaire de la partie décimale : \subitem La
    mantisse peut contenir \texttt{23bits}, et la partie entière
    n'est pas normalisable et n'occupe donc rien sur l'espace de la
    mantisse, nous avons donc \texttt{23bits} disponibles pour
    représenter la partie décimale. Or, nous pouvons dors et déjà
    remarquer que $0.1234 < 2^{-4}$, donc nous aurons très
    certainement un décalage de 4 bits vers la droite au moment de la
    normalisation. Ainsi, nous pouvons nous assurer d'une bonne
    approximation du nombre en réalisant $23 + 4 = 27$ itérations.
    \begin{description}
      \item[méthode des divisions euclidiennes successives]
      \begin{enumerate}
        \item Identification de la valeur du dividende :
        \[ 0.1234 \times 2^{27} = 16562467.6352 \] avec l'arrondi à
        l'entier le plus proche, nous n'avons plus qu'à convertir la
        valeur $ \overline{16562468}_{10} $ en binaire (c'est à ce
        moment là que l'approximation du nombre se fait)

        \item Succesion des divisions
        \DivisionEuclidienneSuccessive[27]{16562468}{2}%

        \subparagraph{Note :} les trois dernières divisions sont
        nécessaires puisque nous avons fait un décalage de 27 bits (en
        multipliant notre partie décimale par $2^{27}$) et nous devons
        réaliser 27 divisions euclidiennes.

        \item Récriture complète du nombre décimale en binaire
        \[ \overline{-0.1234}_{10} =
          \overline{-0.000111111001011100100100100}_{2}\]
      \end{enumerate}

      \item[méthode des multiplications par 2]
      \begin{enumerate}
        \item Succession des multiplications par 2 en conservant la
        partie entière comme valeur pour le \texttt{bit} correspondant
        à l'itération, puis effectuer le calcul de l'itération
        suivante avec la partie décimale. L'arrondi est fait au
        \texttt{bit} supérieur si le \texttt{bit} de la multiplication
        suivante est à \texttt{1}.

        \MulSuccessiveDecBin{0.1234}{28}%
        \subparagraph{Note :} la valeur binaire ici trouvée diffère de
        celle trouvée avec la méthode des divisions successives. En
        effet, nous n'avons pas encore pris soin d'effectuer l'arrondi
        du fait de la valeur de l'itération 28.

        \item Récriture complète du nombre décimale en binaire
        \[ \overline{-0.1234}_{10} =
          \overline{-0.000111111001011100100100011}_{2}\] On
        conservera la valeur arrondie
        \[ \overline{-0.000111111001011100100100100}_{2}\] du fait que
        l'itération 28 fournie un \texttt{bit} à \texttt{1}.
      \end{enumerate}

    \end{description}

    \item Le nombre n'est pas normalisé : normalisation le. On veut le
    nombre sous la forme $ S \times 1.M \times 2^{e} $
    \[ \overline{-0.000111111001011100100100100}_{2} = \overline{-1
        \times 1.11111001011100100100100 \times 2^{-4}}_{2}\] o\`u
    \[ S = \overline{1}_{2} \textnormal{, car le signe est négatif} \]
    \[ e = -4 \Rightarrow E = e + 127 = 123 =
      \overline{01111011}_{2} \]
    \[ M = \overline{11111001011100100100100}_{2} \]
  \end{enumerate}

  \subparagraph{Conclusion}
  \[ \overline{-0.1234}_{10} \simeq 10111101111111001011100100100100
    \textnormal{ (norme IEEE 754, 32bits)} \]

  \subparagraph{Vérification et mis en évidence de l'approximation}
  \[ S = \overline{1}_{2} \textnormal{, car le signe est négatif} \]
  \[ e = -4 \Leftarrow E = e + 127 = 123 = \overline{01111011}_{2} \]
  \[ M = \overline{11111001011100100100100}_{2} \] donc
  \[ -1.11111001011100100100100 \times 2^{-4} \]
  \begin{multline}
    \overline{-0.000111111001011100100100100}_{2} = 2^{-4} + 2^{-5} +
    2^{-6} \\+ 2^{-7} + 2^{-8} + 2^{-9} + 2^{-12} + 2^{-14} + 2^{-15}
    + 2^{-16} + 2^{-19} + 2^{-22} + 2^{-25}
  \end{multline}
  \[ \overline{-0.000111111001011100100100100}_{2} =
    \overline{0.1234000027179718}_{10} \simeq
    \overline{-0.1234}_{10} \]

\end{proof}

\section{Repères}
Les \tablename~\vrefrange{tab_puissances_2_01}{tab_nombres_0_15} vous
rappellent quelques valeurs utiles.

\begin{table}[p]
  \centering
  \begin{tabular}{l@{$=$}rp{1cm}l@{$=$}r@{$=$}r@{k}p{1cm}l@{$=$}r@{$=$}r@{M}}
    $2^0$ & $\numprint{1}$ & & $2^{10}$ & $\numprint{1024}$ & 1 & & $2^{20}$ & $\numprint{1048576}$ & 1 \\
    $2^1$ & $\numprint{2}$ & & $2^{11}$ & $\numprint{2048}$ & 2 & & $2^{21}$ & $\numprint{2097152}$ & 2 \\
    $2^2$ & $\numprint{4}$ & & $2^{12}$ & $\numprint{4096}$ & 4 & & $2^{22}$ & $\numprint{4194304}$ & 4 \\
    $2^3$ & $\numprint{8}$ & & $2^{13}$ & $\numprint{8192}$ & 8 & & $2^{23}$ & $\numprint{8388608}$ & 8 \\
    $2^4$ & $\numprint{16}$ & & $2^{14}$ & $\numprint{16384}$ & 16 & & $2^{24}$ & $\numprint{16777216}$ & 16 \\
    $2^5$ & $\numprint{32}$ & & $2^{15}$ & $\numprint{32768}$ & 32 & & $2^{25}$ & $\numprint{33554432}$ & 32 \\
    $2^6$ & $\numprint{64}$ & & $2^{16}$ & $\numprint{65536}$ & 64 & & $2^{26}$ & $\numprint{67108864}$ & 64 \\
    $2^7$ & $\numprint{128}$ & & $2^{17}$ & $\numprint{131072}$ & 128 & & $2^{27}$ & $\numprint{134217728}$ & 128 \\
    $2^8$ & $\numprint{256}$ & & $2^{18}$ & $\numprint{262144}$ & 256 & & $2^{28}$ & $\numprint{268435456}$ & 256 \\
    $2^9$ & $\numprint{512}$ & & $2^{19}$ & $\numprint{524288}$ & 512 & & $2^{29}$ & $\numprint{536870912}$ & 512 \\
  \end{tabular}
  \caption{Les puissances de 2 jusqu'à
    \texttt{512M}}\label{tab_puissances_2_01}
\end{table}

\begin{table}[p]
  \centering
  \begin{tabular}{l@{$=$}r@{$=$}r@{G}}
    $2^{30}$ & $\numprint{1073741824}$ & 1 \\
    $2^{31}$ & $\numprint{2147483648}$ & 2 \\
    $2^{32}$ & $\numprint{4294967296}$ & 4 \\
    $2^{33}$ & $\numprint{8589934592}$ & 8 \\
    $2^{34}$ & $\numprint{17179869184}$ & 16 \\
    $2^{35}$ & $\numprint{34359738368}$ & 32 \\
    $2^{36}$ & $\numprint{68719476736}$ & 64 \\
    $2^{37}$ & $\numprint{137438953472}$ & 128 \\
    $2^{38}$ & $\numprint{274877906944}$ & 256 \\
    $2^{39}$ & $\numprint{549755813888}$ & 512 \\
  \end{tabular}
  \caption{Les puissances de 2, de \texttt{1G} à
    \texttt{512G}}\label{tab_puissances_2_02}
\end{table}

\begin{table}[p]
  \centering
  \begin{tabular}{ccc}
    $10^{1}$ & déca & da \\
    $10^{2}$ & hecto & h \\
    $10^{3}$ & kilo & k \\
    $10^{6}$ & méga & M \\
    $10^{9}$ & giga & G \\
    $10^{12}$ & téra & T \\
    $10^{15}$ & péta & P \\
    $10^{18}$ & exa & E \\
  \end{tabular}
  \caption{Les puissances de 10}\label{tab_puissances_10}
\end{table}

\begin{table}[p]
  \centering
  \begin{tabular}{|p{2.5cm}|p{2.5cm}|p{2.5cm}|p{2.5cm}|}
    \hline
    \textbf{Binaire} & \textbf{Octal} & \textbf{Hexadécimal} & \textbf{Décimal} \\ \hline
    \nplpadding{4}\nbaseprint{0b0} & \nplpadding{2}\nbaseprint{0o0} & \nplpadding{1}\nbaseprint{0x0} & \nplpadding{2}\nbaseprint{0d0} \\ \hline
    \nplpadding{4}\nbaseprint{0b1} & \nplpadding{2}\nbaseprint{0o1} & \nplpadding{1}\nbaseprint{0x1} & \nplpadding{2}\nbaseprint{0d1} \\
    \hline \nplpadding{4}\nbaseprint{0b10} & \nplpadding{2}\nbaseprint{0o2} & \nplpadding{1}\nbaseprint{0x2} & \nplpadding{2}\nbaseprint{0d2} \\ \hline
    \nplpadding{4}\nbaseprint{0b11} & \nplpadding{2}\nbaseprint{0o3} & \nplpadding{1}\nbaseprint{0x3} & \nplpadding{2}\nbaseprint{0d3} \\
    \hline \nplpadding{4}\nbaseprint{0b100} & \nplpadding{2}\nbaseprint{0o4} & \nplpadding{1}\nbaseprint{0x4} & \nplpadding{2}\nbaseprint{0d4} \\ \hline
    \nplpadding{4}\nbaseprint{0b101} & \nplpadding{2}\nbaseprint{0o5} & \nplpadding{1}\nbaseprint{0x5} & \nplpadding{2}\nbaseprint{0d5} \\
    \hline \nplpadding{4}\nbaseprint{0b110} & \nplpadding{2}\nbaseprint{0o6} & \nplpadding{1}\nbaseprint{0x6} & \nplpadding{2}\nbaseprint{0d6} \\ \hline
    \nplpadding{4}\nbaseprint{0b111} & \nplpadding{2}\nbaseprint{0o7} & \nplpadding{1}\nbaseprint{0x7} & \nplpadding{2}\nbaseprint{0d7} \\
    \hline \nplpadding{4}\nbaseprint{0b1000} & \nplpadding{2}\nbaseprint{0o10} & \nplpadding{1}\nbaseprint{0x8} & \nplpadding{2}\nbaseprint{0d8} \\ \hline
    \nplpadding{4}\nbaseprint{0b1001} & \nplpadding{2}\nbaseprint{0o11} & \nplpadding{1}\nbaseprint{0x9} & \nplpadding{2}\nbaseprint{0d9} \\
    \hline \nplpadding{4}\nbaseprint{0b1010} & \nplpadding{2}\nbaseprint{0o12} & \nplpadding{1}\nbaseprint{0xA} & \nplpadding{2}\nbaseprint{0d10} \\ \hline
    \nplpadding{4}\nbaseprint{0b1011} & \nplpadding{2}\nbaseprint{0o13} & \nplpadding{1}\nbaseprint{0xB} & \nplpadding{2}\nbaseprint{0d11} \\ \hline
    \nplpadding{4}\nbaseprint{0b1100} & \nplpadding{2}\nbaseprint{0o14} & \nplpadding{1}\nbaseprint{0xC} & \nplpadding{2}\nbaseprint{0d12} \\ \hline
    \nplpadding{4}\nbaseprint{0b1101} & \nplpadding{2}\nbaseprint{0o15} & \nplpadding{1}\nbaseprint{0xD} & \nplpadding{2}\nbaseprint{0d13} \\ \hline
    \nplpadding{4}\nbaseprint{0b1110} & \nplpadding{2}\nbaseprint{0o16} & \nplpadding{1}\nbaseprint{0xE} & \nplpadding{2}\nbaseprint{0d14} \\ \hline
    \nplpadding{4}\nbaseprint{0b1111} & \nplpadding{2}\nbaseprint{0o17} & \nplpadding{1}\nbaseprint{0xF} & \nplpadding{2}\nbaseprint{0d15} \\ \hline
  \end{tabular}
  \caption{Les seize premiers nombres (de 0 à
    15)}\label{tab_nombres_0_15}
\end{table}

\clearpage
\printbibheading{}
% \printbibliography[nottype=online,check=notonline,heading=subbibliography,title={Bibliographiques}]
\printbibliography[check=online,heading=subbibliography,title={Webographiques}]
\printglossaries{}
\end{document}
